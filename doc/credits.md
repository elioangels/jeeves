# jeeves Credits

## Artwork

- [Lunch-is-served](https://publicdomainvectors.org/en/free-clipart/Lunch-is-served-icon/83088.html)
- [Mans-disguise](https://publicdomainvectors.org/en/free-clipart/Mans-disguise/71365.html)

## Core Thanks!

- [elioWay](https://elioway.gitlab.io)
- [Yeoman](http://yeoman.io/)

## Worth a look

- <https://github.com/cogniteev/ember-cli-file-saver>
- <https://github.com/mike-north/ember-api-actions>
- https://ramdajs.com/docs/#where
