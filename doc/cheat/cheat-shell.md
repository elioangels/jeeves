# shell commands

## ip

```shell
hostname -I
ifconfig
curl https://ipinfo.io/ip
```

## list

```shell
ls -a
# Directories ordered by size
du -hs * | sort -hr
# See permissions
ls -lh
#
```

## find

```shell
# delete empties
find . -type d -empty -delete
# delete empties
find .  -name  -type f
```

## apt

```shell
sudo dpkg -i ./name.deb
sudo apt-get install -f
# OR
sudo apt install ./name.deb


sudo apt-get update --fix-missing
sudo apt-get upgrade -y

sudo apt purge <package> -y

sudo apt autoremove -y
sudo apt clean

sudo apt --fix-broken install -y
```

### Recently installed

```shell
grep -w install /var/log/dpkg.log
```

### Keys

```shell
sudo apt-key adv --refresh-keys --keyserver keyserver.ubuntu.com
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 76F1A20FF987672F
```

## services

```shell
sudo service --status-all

sudo -i service servicename stop
sudo -i service servicename start
sudo -i service servicename restart

sudo -i systemctl disable servicename
sudo -i systemctl enable servicename

sudo initctl list
```

## history

```shell
nano  ~/.inputrc
```

**Paste**::

```
"\e[A": history-search-backward
"\e[B": history-search-forward
"\e[C": forward-char
"\e[D": backward-char
```

## users

```shell
sudo adduser cm
sudo usermod -aG sudo cm

# login as new user to change password
passwd

# To remove/delete a user, first you can use:
sudo userdel username

# Then you may want to delete the home directory for the deleted user account :
sudo rm -r /home/username
# (Please use with caution the above command!)

# To modify the username of a user:
usermod -l new_username old_username

# To change the password for a user:
sudo passwd username
```

## rsync

<https://www.digitalocean.com/community/tutorials/how-to-use-rsync-to-sync-local-and-remote-directories>

```shell
echo "node_modules
venv*
venv" > ~/.cvsignore
# `-a` = `-r` + more
rsync -a dir1/ dir2 --cvs-exclude
# Backup
rsync -amP dir1/ dir2/ --delete --cvs-exclude
# copies contents of dir1 into dir2, i.e. dir1/a dir1/b => dir2/a dir2/b
rsync -a dir1 dir2
# copies dir1 into dir2, i.e. dir1/a dir1/b => dir2/dir1/a dir2/dir1/b
# `--cvs-exclude                 ignore `.git` and stuff and `~/.cvsignore`
# `-v` verbose
# `-n` `--dry-run`
# `-P` `--progress` and `--partial`
# `-R` `--relative`
# `-m` `--prune-empty-dirs`
# `--delete`  remove files no longer in source
# `--exclude=pattern_to_exclude`
# `--include=pattern_to_include`
# `--ignore-existing`        skip updating files that exist on receiver
# `--remove-source-files`    sender removes synchronized files (non-dir)
rsync -aP dir1/ username@remote_host:destination_directory --cvs-exclude --delete
# Veracrypt or other where files are modified due to encryption
# -u, --update                skip files that are newer on the receiver
# -t, --times                 preserve modification times
```

## sftp

```shell
sftp user@host
cd path                            Change remote directory to 'path'
lcd path                           Change local directory to 'path'
get [-afpR] remote [local]         Download file
put [-afpR] local [remote]         Upload file
bye                                Quit sftp

# More Commands
chgrp [-h] grp path                Change group of file 'path' to 'grp'
chmod [-h] mode path               Change permissions of file 'path' to 'mode'
chown [-h] own path                Change owner of file 'path' to 'own'
df [-hi] [path]                    Display statistics for current directory or filesystem containing 'path'
exit                               Quit sftp
help                               Display this help text
lls [ls-options [path]]            Display local directory listing
lmkdir path                        Create local directory
ln [-s] oldpath newpath            Link remote file (-s for symlink)
lpwd                               Print local working directory
ls [-1afhlnrSt] [path]             Display remote directory listing
lumask umask                       Set local umask to 'umask'
mkdir path                         Create remote directory
progress                           Toggle display of progress meter
pwd                                Display remote working directory
quit                               Quit sftp
reget [-fpR] remote [local]        Resume download file
rename oldpath newpath             Rename remote file
reput [-fpR] local [remote]        Resume upload file
rm path                            Delete remote file
rmdir path                         Remove remote directory
symlink oldpath newpath            Symlink remote file
version                            Show SFTP version
!command                           Execute 'command' in local shell
!                                  Escape to local shell
?                                  Synonym for help
```

## zip

```shell
zip -r -0 -u invoices.zip invoices/
# encrypt
zip -r -e safe.zip unsafed/
```

## format

```shell
mkntfs -Q -v -F -L /dev/sdb1
```
