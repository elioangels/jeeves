# Cheat npm

## **Publish**

```shell
npm version patch
npm version minor
npm publish
# First time
npm publish --access public
```

## **Package**

```shell
# Install local
npm i package

# Update local
npm i package@latest

# List + global
npm ls --depth=0
npm ls --depth=0 -g

# Update all packages
npm i -g ember-cli-update
ncu -u
npm i
```

## **File Install**

```
npm i file:../phaser3-timesup/
```

## **Global Install**

```
# Install global
npm i -g package

# List global
npm list -g --depth=0
```

## **Global Install**

```
# Install global
npm i -g package

# List global
npm list -g --depth=0
```

## **elioWay Globals**

- @elioway/chisel
- @feathersjs
- ember-cli
- ember-cli-update
- generator-sin
- generator-thing
- gulp-cli
- npm
- npm-check-updates
- prettier
- ts-node
- typescript
- yo
