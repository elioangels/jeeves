# Cheat Kimsufi

- <https://forum.kimsufi.com/showthread.php/21102-Nameserver-configuration-help>

## Certbot + Lets Encrypt

```
sudo apt install certbot python3-certbot-nginx
sudo certbot --nginx -d ns3267077.ip-5-39-80.eu
```

```
- Congratulations! Your certificate and chain have been saved at:
  /etc/letsencrypt/live/ns3267077.ip-5-39-80.eu/fullchain.pem
  Your key file has been saved at:
  /etc/letsencrypt/live/ns3267077.ip-5-39-80.eu/privkey.pem
  Your cert will expire on 2021-12-06\. To obtain a new or tweaked
  version of this certificate in the future, simply run certbot again
  with the "certonly" option. To non-interactively renew *all* of
  your certificates, run "certbot renew"
```

## DNS

1. Add domain names to Secondary DNS in Kimsufi DNS Dashboard. You'll need to be able to add TXT records to your domain hosting config. In Namecheap that means using Default DNS for this step and doing it first.
2. Once Secondary DNS is setup, change the domain hosting DNS config to the following NS:

  - `ns3267077.ip-5-39-80.eu`
  - `ns.kimsufi.com`

3. `213.186.33.199` is Kimsufi's IP address.

4. Yours will be listed in the Dashboard.

## Bind

In`/etc/bind/named.conf.local` you need to define two zones:

```
zone "elioway.com" {
        type master;
        file "/etc/bind/db.elioway.com";
        allow-transfer {213.186.33.199;};
};

zone "5.39.80.70.in-addr.arpa" {
        type master;
        file "/etc/bind/db.5";
        allow-transfer {213.186.33.199;};
};
```

The first zone is the 'forward' zone, which is the way in which domain names are translated into IP addresses. The file specified there is what we're going to create, and you can name it whatever you want. The 'allow-transfer' setting is where we specify the secondary nameserver, i.e. the IP address of ns.kimsufi.com we noted above (in this case 213.186.33.199).

The second zone is the 'reverse' zone, which translates an IP address into (a) domain name(s). Again, the secondary nameserver is specified here, and the zone is conventionally named after the ARPA website that we will later point the zone to (I think it can be named whatever you wish, though). A good practice is also to name the file specified here after the last octet of your Kimsufi server's IP address. In my case this is 78, so I've named the file db.78.

So, create and edit `/etc/bind/db.elioway.com` (or whatever you called it), and put the following stuff in it: Code:

```
$TTL 12H
$ORIGIN elioway.com.
@                       IN              SOA             ns3267077.ip-5-39-80.eu. root.elioway.com. (
                        202109071057    ; Serial
                        8H              ; Refresh
                        30M             ; Retry
                        4W              ; Expire
                        8H              ; Minimum TTL
)
                        IN              NS              ns3267077.ip-5-39-80.eu.
                        IN              NS              ns.kimsufi.com.
                        IN              MX      10      mail.elioway.com.
elioway.com.            IN              A               5.39.80.70
ns                      IN              A               5.39.80.70
mail                    IN              A               5.39.80.70
www                     IN              CNAME           elioway.com.
```

Replace the following:

- `elioway.com` with your domain
- `ns3267077.ip-5-39-80.eu.` with your Kimsufi hostname
- `root.elioway.com.` with an email address for your domain (don't use @, instead use a full stop)
- `202109071057` with a serial number of your choice. Good practice is to use the current date with a two digit serial number on the end representing the number of times you've edited the file that day. You should change this serial number every time you edit this file, to avoid issues with other DNS servers.
- `mail.elioway.com`. with the subdomain you want to handle mail with (or delete this row entirely if you don't want to use mail).
- `5.39.80.70` with your server's IP address - the one you made a note of from the drop-down box earlier.

Remember to keep the last full stop intact in any domains you change in the code above. The last full stop designates that the domain is a fully qualified domain name and not relative to some other domain. You'll get all sorts of problems if you're not careful.

The CNAME entry allows www.elioway.com to point to elioway.com. It's very much advised to keep this if you're running an ordinary website - almost every other hosting provider automatically does this, so you'll confuse your users if you don't also offer the same ability.

Next, create and edit the /etc/bind/db.5 (or whatever you called it) file you specified earlier: Code:

```
$TTL 12H
@          IN              SOA             ns3267077.ip-5-39-80.eu. root.elioway.com. (
           2014012602      ; Serial
           8H              ; Refresh
           30M             ; Retry
           4W              ; Expire
           8H              ; Minimum TTL
)
           IN NS   ns3267077.ip-5-39-80.eu.
           IN NS   ns.kimsufi.com.
           IN PTR  elioway.com.
```

Again, change the relevant details to your own equivalents.

Edit /etc/bind/named.conf.options and comment out the listen-on { 127.0.0.1; }; line. I am not sure if this is necessary, but other guides do it so I am inclined to follow their advice.

Restart BIND:

```
sudo service bind9 restart
```

If there are no errors in either the restart message or in /var/log/syslog, then you're all good.

```
sudo tail -f /var/log/syslog
```

If there are errors, check your configuration file for missing semicolons and so on. Otherwise, Googling the error message can often bring up useful results.

## dnsinspect

- <https://www.dnsinspect.com/elioway.com/10579194>
