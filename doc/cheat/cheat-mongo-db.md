# Cheat mongoDB

## Installing

- [eliobones/mongo-db/Quickstart](eliobones/mongo-db/doc/quickstart)

Or

Bones uses mongoDB as its database. These notes are taken from the following documentation:

- [mongodb Install](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)

```shell
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -

echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y mongodb-org
```

## Purge

```shell
sudo rm /var/lib/apt/lists/*mongodb-org_4.0*
sudo rm /etc/apt/sources.list.d/mongodb-org-4.0.list
sudo rm /var/lib/mongodb
```

## Service Control

```shell
sudo service mongod start
sudo service mongod stop
sudo service mongod restart

sudo systemctl status mongod
sudo systemctl enable mongod
sudo systemctl disable mongod
```

# Usage

```shell
sudo mongo
```

Enter your sudo password then:

```shell
show dbs
```

If this works, you're good to go.

To exit:

```shell
exit
```

Dropping a db:

```shell
use jsonApi
# switched to db jsonApi
db.dropDatabase()
```

Get data direct from mongo:

```shell
db.getCollection('Thing')
```

## Mongo issues

Post Install: Doesn't start or can't find it

If you get an error like "I can't see a server":

```
sudo systemctl unmask mongodb
> Unit mongodb.service does not exist, proceeding anyway.
sudo service mongod start
```
