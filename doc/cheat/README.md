# Cheats

Point of principle. List what you don't want to forget, not things you have yet to remember. List cheats of things you have already learnt, and DON'T copy and paste third party cheat sheets of examples you don't already know. Add things to `jeeves/doc/cheats/` when you first go through a source cheat sheet - or even better when you are coding (crediting stackoverflow answers, etc) and only list the things you "don't want to forget".
