# Installing jeeves

- [Prerequisites](./prerequisites)

## npm

Install into your SASS projects.

```
npm install @elioway/jeeves
yarn add @elioway/jeeves
```

## Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git clone https://gitlab.com/elioangels/jeeves.git
```
