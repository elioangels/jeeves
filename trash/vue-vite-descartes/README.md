![](https://elioway.gitlab.io/elioenlightenment/descartes/elio-descartes-logo.png)

> I think, therefore I am, **the elioWay**

Paused to try vue-router.

# descartes

Jacque's Seven Ages, a figurative poem, the elioWay.

- [descartes Documentation](https://elioway.gitlab.io/elioenlightenment/descartes/)

## Installing

- [Installing descartes](https://elioway.gitlab.io/elioenlightenment/descartes/installing.html)

## Requirements

- [descartes Prerequisites](https://elioway.gitlab.io/descartes/installing.html)

## Seeing is Believing

```
npm i
npm run dev
```

- [descartes Quickstart](https://elioway.gitlab.io/descartes/quickstart.html)
- [descartes Quickstart](https://elioway.gitlab.io/elioenlightenment/descartes/quickstart.html)

# Credits

- [descartes Credits](https://elioway.gitlab.io/elioenlightenment/descartes/credits.html)

## License

[MIT](license)

![](https://elioway.gitlab.io/elioenlightenment/descartes/apple-touch-icon.png)
