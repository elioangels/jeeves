# Installing descartes

## Prerequisites

- [elioWay Prerequisites](https://elioway.gitlab.io/installing.html)
- [descartes Prerequisites](https://elioway.gitlab.io/descartes/installing.html)

## npm

Install into your SASS projects.

```
npm install @elioway/descartes
yarn add @elioway/descartes
```

## Development

```bash
cd elioway
git clone https://gitlab.com/elioway/descartes.git
cd descartes
git clone https://gitlab.com/descartes/descartes.git
```
