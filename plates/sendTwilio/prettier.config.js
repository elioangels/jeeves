// prettier.config.js or .prettierrc.js
module.exports = {
  arrowParens: "always",
  semi: false,
  printWidth: 80,
}
