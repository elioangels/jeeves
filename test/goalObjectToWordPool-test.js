const should = require("chai").should()
const { chunk, clamp, flatMap, merge, sortBy } = require("lodash")

/** Non-random shuffle of a non-unique list which ensures any "apple" is as far away as possible from other "apple"s, mixed in with other fruits.*/
function goalObjectToWordPool(goalObject) {
  const perfectShuffle = (acc, [word, n]) => {
    const perfectZipping = (a1, a2) => {
      let mapped = a1.map((x, i) => {
        if (i < a2.length) {
          a1[i].push(a2[i])
        }
        return a1[i]
      })
      return flatMap(mapped)
    }
    const wordListForGoal = new Array(n).fill(word)
    if (!acc || !acc.length) {
      return wordListForGoal
    } else {
      let accumulatorChunked = chunk(acc, Math.round(acc.length / (n + 1)))
      return perfectZipping(accumulatorChunked, wordListForGoal)
    }
  }
  let shuffled = Object.entries(goalObject)
    .sort((a, b) => b[1] - a[1])
    .reduce(perfectShuffle, [])
  return shuffled
}

describe("Function: perfectShuffle", () => {
  it("defaults", () => {
    goalObjectToWordPool({ a: 1, b: 2, c: 3 }).should.eql([
      "c",
      "b",
      "c",
      "a",
      "b",
      "c",
    ])
    goalObjectToWordPool({ a: 1, b: 2, c: 3, d: 4 }).should.eql([
      "d",
      "c",
      "b",
      "d",
      "c",
      "a",
      "b",
      "d",
      "c",
      "d",
    ])
    goalObjectToWordPool({ a: 1, b: 2, c: 3, d: 4, e: 5 }).should.eql([
      "e",
      "d",
      "c",
      "e",
      "b",
      "d",
      "c",
      "a",
      "e",
      "d",
      "b",
      "c",
      "e",
      "d",
      "e",
    ])
    goalObjectToWordPool({ a: 3, b: 3, c: 3 }).should.eql([
      "a",
      "b",
      "c",
      "a",
      "b",
      "c",
      "a",
      "b",
      "c",
    ])
    goalObjectToWordPool({ a: 3, b: 3, c: 30 }).should.eql([
      "c",
      "c",
      "c",
      "c",
      "c",
      "c",
      "c",
      "c",
      "b",
      "a",
      "c",
      "c",
      "c",
      "c",
      "c",
      "c",
      "c",
      "b",
      "c",
      "a",
      "c",
      "c",
      "c",
      "c",
      "c",
      "c",
      "b",
      "c",
      "c",
      "a",
      "c",
      "c",
      "c",
      "c",
      "c",
      "c",
    ])
    goalObjectToWordPool({ a: 3, b: 10, c: 1 }).should.eql([
      "b",
      "b",
      "b",
      "a",
      "b",
      "b",
      "b",
      "c",
      "a",
      "b",
      "b",
      "b",
      "a",
      "b",
    ])
  })
  it("Issues fixed", () => {
    goalObjectToWordPool({
      ";": 2,
      a: 2,
      d: 3,
      f: 3,
      j: 3,
      k: 3,
      l: 2,
      s: 2,
    }).should.eql([
      "d",
      "f",
      "k",
      "j",
      ";",
      "l",
      "s",
      "a",
      "d",
      "k",
      "f",
      "j",
      "l",
      "s",
      ";",
      "a",
      "k",
      "d",
      "f",
      "j",
    ])
  })
})
