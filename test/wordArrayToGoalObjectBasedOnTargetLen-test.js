const should = require("chai").should()
const { chunk, clamp, flatMap, merge, sortBy } = require("lodash")

const wordArrayToGoalObjectBasedOnTargetLen = (wordArray, targetLen) => {
  // Default targetLen
  targetLen = targetLen || wordArray.length
  // Target smaller than word list?
  if (targetLen < wordArray.length) {
    // We must slice!
    wordArray = wordArray.slice(0, targetLen)
  }
  let remainder = targetLen % wordArray.length
  let wordTargWhole = (targetLen - remainder) / wordArray.length
  let wholeCount = wordArray
    .map(word => [word, wordTargWhole]) // Give each item in list the wholeDiv.
    .reduce((acum, [word, wordTarg]) => {
      // Compress duplicates down into a single dictionary entry.
      acum[word] = (acum[word] || 0) + wordTarg
      return acum
    }, {})
  let sortedByFewest = Object.entries(wholeCount).sort((a, b) => a[1] - b[1])
  // Spread the remainder across the final list.
  for (let i = 0; i < remainder; i++) {
    sortedByFewest[i % sortedByFewest.length][1] += 1
  }
  // Convert list to dict and return
  return Object.fromEntries(sortedByFewest)
}

describe("Function: wordArrayToTargetLen", () => {
  it("defaults", () => {
    wordArrayToGoalObjectBasedOnTargetLen("abcdef".split(""), 2).should.eql({
      a: 1,
      b: 1,
    })
    wordArrayToGoalObjectBasedOnTargetLen("ab".split(""), 2).should.eql({
      a: 1,
      b: 1,
    })
    wordArrayToGoalObjectBasedOnTargetLen("ab".split(""), 4).should.eql({
      a: 2,
      b: 2,
    })
    wordArrayToGoalObjectBasedOnTargetLen("ab".split(""), 3).should.eql({
      a: 2,
      b: 1,
    })
    wordArrayToGoalObjectBasedOnTargetLen("abc".split(""), 10).should.eql({
      a: 4,
      b: 3,
      c: 3,
    })
    wordArrayToGoalObjectBasedOnTargetLen("abcc".split(""), 10).should.eql({
      a: 3,
      b: 3,
      c: 4,
    })
    Array.from(["abcccc", "ccccab", "ccabcc"]).forEach(arr => {
      wordArrayToGoalObjectBasedOnTargetLen(arr.split(""), 10).should.eql({
        a: 3,
        b: 2,
        c: 5,
      })
    })
  })
})
