const should = require("chai").should()

const axis = require("../axis/index.js")
let testH, testW, testDv, expectW, expectH
;[testW, testH, testDv] = [150, 50, 5]
;[expectW, expectH, expectX, expectY] = [30, 10, 75, 25]

describe("Function: axis", () => {
  it("defaults", () => {
    let { H, W, X, Y } = axis(testW, testH, testDv)
    H().should.eql(expectH)
    W().should.eql(expectW)
    X().should.eql(expectX)
    Y().should.eql(expectY)
  })
  it("H(n) height in factors of n", () => {
    let { H } = axis(testW, testH, testDv)
    H().should.eql(expectH)
    H(1).should.eql(H())
    H(2).should.eql(H() * 2)
    H(3).should.eql(H() * 3)
    H(4).should.eql(H() * 4)
    H(5).should.eql(H() * 5)
  })
  it("W(n) width in factors of n", () => {
    let { W } = axis(testW, testH, testDv)
    W().should.eql(expectW)
    W(1).should.eql(W())
    W(2).should.eql(W() * 2)
    W(3).should.eql(W() * 3)
    W(4).should.eql(W() * 4)
    W(5).should.eql(W() * 5)
  })
  it("X(n) centre along x which n shifts", () => {
    let { W, X } = axis(testW, testH, testDv)
    X().should.eql(expectX)
    X(1).should.eql(X() + W())
    X(2).should.eql(X() + W() + W())
    X(0).should.eql(X())
    X(-2).should.eql(X() - W() - W())
    X(-1).should.eql(X() - W())
  })
  it("X(n, obj) shifts obj centre along x which n shifts", () => {
    let { W, X } = axis(testW, testH, testDv)
    let w = 33
    X(0, { width: w, scaleX: 1 }).should.eql(expectX - w / 2)
    X(0, { width: w, scaleX: 0.5 }).should.eql(expectX - (w / 2) * 0.5)
  })
  it("Y(n) centre along y which n shifts", () => {
    let { H, Y } = axis(testW, testH, testDv)
    Y().should.eql(expectY)
    Y(1).should.eql(Y() + H())
    Y(2).should.eql(Y() + H() + H())
    Y(0).should.eql(Y())
    Y(-2).should.eql(Y() - H() - H())
    Y(-1).should.eql(Y() - H())
  })
  it("Y(n, obj) shifts obj centre along y which n shifts", () => {
    let { W, Y } = axis(testW, testH, testDv)
    let w = 33
    Y(0, { height: w, scaleY: 1 }).should.eql(expectY - w / 2)
    Y(0, { height: w, scaleY: 0.5 }).should.eql(expectY - (w / 2) * 0.5)
  })
  it("X(3) ", () => {
    ;[testW, testH, testDv] = [100, 80, 3]
    ;[expectW, expectH, expectX, expectY] = [33, 27, 50, 40]
    let { H, W, X, Y } = axis(testW, testH, testDv)
    H().should.equal(expectH)
    W().should.equal(expectW)
    X().should.equal(expectX)
    Y().should.equal(expectY)
  })
})
