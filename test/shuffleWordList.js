const should = require("chai").should()
const { chunk, clamp, flatMap, merge, sortBy } = require("lodash")

function shuffleWordList(wordList) {
  /** Produce the fewest repetitions */
  const perfectShuffle = (acc, [word, n]) => {
    /** Zipper Up two lists (can be different sizes). */
    const perfectZipping = (a1, a2) => {
      let zipped = a1.map((x, i) => {
        if (i < a2.length) {
          a1[i].push(a2[i])
        }
        return a1[i]
      })
      return flatMap(zipped)
    }
    // List the given word n times.
    const wordListForGoal = new Array(n).fill(word)
    if (!acc || !acc.length) {
      console.log(word, "Stage1", wordListForGoal)
      return wordListForGoal
    } else {
      console.log(word, "StageX", wordListForGoal)
      // Zip the new list into the accumulator of the JS `reduce` func.
      let accumulatorChunked = chunk(acc, Math.round(acc.length / (n + 1)))
      let perzip = perfectZipping(accumulatorChunked, wordListForGoal)
      console.log(word, "StageX perfectZipping", perzip)
      return perzip
    }
  }
  // For each word in the goalPerWord.
  return wordList.sort((a, b) => a - b)
  // .reduce(perfectShuffle, [])
  // .sort(() => (Math.random() < 0.3 ? -1 : 1))
}

describe("Function: perfectShuffle", () => {
  it.only("defaults", () => {
    shuffleWordList([1, 1, 2, 1, 3]).should.eql([1, 2, 1, 3, 1])
    // shuffleWordList([1, 1, 2, 2, 3, 1, 1, ]).should.eql([1,  2, 1, 3, 2, 1])
  })
})
