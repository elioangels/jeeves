const should = require("chai").should()

function cloneOfSimpleObject(obj) {
  let rtn = {}
  for (let [k, v] of Object.entries(obj)) {
    rtn[k] = v
  }
  return rtn
}

describe("Function: cloneOfSimpleObject", () => {
  it("Breaks reference to simple object.", () => {
    let orig = new Map()
    orig["a"] = 1
    orig["b"] = 2
    orig["c"] = { d: 4, e: 5 }
    let copyOrig = orig
    copyOrig.a.should.eql(1)
    orig.a = 3
    orig.c.d = 99
    copyOrig.a.should.be.eql(3)
    copyOrig.c.d.should.be.eql(99)
    // Clone orig
    copyOrig = cloneOfSimpleObject(orig)
    orig.a = 1
    orig.c.d = 199
    // Property `a` reference is broken.
    copyOrig.a.should.still.eql(3)
    // Property `c` is an object and the reference is not broken.
    copyOrig.c.d.should.be.eql(199)
  })
})
