![](https://elioway.gitlab.io/elioangels/elio-jeeves-logo.png)

# jeeves

**DONE** Moved from `gitlab.io:elioangels/jeeves` repo.

- A playground for running code in multiple languages.
- A place to store snippets you'd like to remember later.
- A source of common cheats/snippets in various languages and modules.

- [jeeves Documentation](https://elioway.gitlab.io/elioangels/jeeves/)

## Installing

- [Installing jeeves](https://elioway.gitlab.io/elioangels/jeeves/installing.html)

## Requirements

- [elioFaithful Prerequisites](https://elioway.gitlab.io/elioangels/installing.html)

## Nutshell

- [jeeves Quickstart](https://elioway.gitlab.io/elioangels/jeeves/quickstart.html)

## License

[MIT](license)

![](apple-touch-icon.png)
